﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AddNEditWindow.xaml
    /// </summary>
    public partial class AddNEditWindow : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Aerodrom aerodrom;
        public AddNEditWindow(Aerodrom aerodrom, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.opcija = opcija;

            this.DataContext = aerodrom;

            if (opcija.Equals(EOpcija.IZMENA))
            {
                TxtSifra.IsEnabled = false;
            }

            
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(EOpcija.DODAVANJE) && !ValidacijaSifra(aerodrom.Sifra) && validacija() == true)
            {
                aerodrom.SacuvajAerodrom();
            }
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }


        private bool ValidacijaSifra(String sifra)
        {
            foreach (Aerodrom aerodrom in Data.Instance.Aerodromi)
            {
                if (aerodrom.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }
        public bool validacija()
        {

            if (TxtNaziv.Text.Equals(""))
            {
                MessageBox.Show("Nije unet naziv");
                return false;
            }
            if (TxtSifra.Text.Equals(""))
            {
                MessageBox.Show("Nije unet sifra");
                return false;
            }
            if (TxtGrad.Text.Equals(""))
            {
                MessageBox.Show("Nije unet grad");
                return false;
            }
            return true;
        }
    }

}
