﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AerodromiWindow.xaml
    /// </summary>
    /// 
    
    public partial class AerodromiWindow : Window
    {

        ICollectionView view;
        public AerodromiWindow()
        {
            InitializeComponent();
            DGAerodromi.ItemsSource = Data.Instance.Aerodromi;
            DGAerodromi.IsSynchronizedWithCurrentItem = true;

            view = CollectionViewSource.GetDefaultView(Data.Instance.Aerodromi);
            view.Filter = CustomFilter;


            DGAerodromi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);



        }
        private bool CustomFilter(Object obj)
        {
            Aerodrom aerodrom = obj as Aerodrom;
            if (txtPretraga.Equals(String.Empty))
            {
               return aerodrom.Aktivan;
            }
            else
            {
                return aerodrom.Aktivan && aerodrom.Naziv.Contains(txtPretraga.Text);
            }
            
        }


    private void BtnDodajAerodrom_Click(object sender, RoutedEventArgs e)
        {
            AddNEditWindow aw = new AddNEditWindow(new Aerodrom());

            aw.ShowDialog();
        }

        private void BtnIzmeniAerodrom_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom selektovanAerodrom = (Aerodrom)DGAerodromi.SelectedItem;
            if (selektovanAerodrom != null)
            {
                Aerodrom stari = (Aerodrom)selektovanAerodrom.Clone();
                AddNEditWindow ew = new AddNEditWindow(selektovanAerodrom, AddNEditWindow.EOpcija.IZMENA);
                if (ew.ShowDialog() != true)
                {
                    int indeks = IndeksAerodroma(selektovanAerodrom.Sifra);
                    Data.Instance.Aerodromi[indeks] = stari;
                }
                else
                {
                    selektovanAerodrom.IzmenaAerodroma();
                    this.DialogResult = false;
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan aerodrom");
            }
            DGAerodromi.Items.Refresh();
        }

        private void BtnObrisiAerodrom_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)DGAerodromi.SelectedItem;
            if (SelektovanAerodrom(aerodrom) && aerodrom.Aktivan) // && aerodrom.Aktivan je visak ali kao
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda",MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    //   int indeks = IndeksAerodroma(aerodrom.Sifra);
                    // Data.Instance.Aerodromi[indeks].Aktivan = false;
                    //Aerodrom selektovanAerodrom = (Aerodrom)DGAerodromi.SelectedItem;
                    // Data.Instance.Aerodromi.Remove(selektovanAerodrom);
                    aerodrom.BrisanjeAerodroma();
                }
            }
        }

        private int IndeksAerodroma(String sifra)
        {
            int indeks = -1;
            for (int i=0; i<Data.Instance.Aerodromi.Count; i++)
            {
                if (Data.Instance.Aerodromi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAerodrom(Aerodrom aerodrom)
        {
            if (aerodrom == null)
            {
                MessageBox.Show("Nije selektovan aerodrom! ");
                return false;
            }
            return true;
        }

        private void BtnPrikaz_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom selektovanAerodrom = (Aerodrom)DGAerodromi.SelectedItem;
            if (selektovanAerodrom != null)
            {

                PrikazAerodroma pa = new PrikazAerodroma(selektovanAerodrom);
                pa.ShowDialog();

            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan aerodrom");
            }
        }

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}

