﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AvioKompanijaAddEdit.xaml
    /// </summary>
    public partial class AvioKompanijaAddEdit : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        public AvioKompanija avioK;


        public AvioKompanijaAddEdit(AvioKompanija avioK, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();

            this.avioK = avioK;
            this.opcija = opcija;

            this.DataContext = avioK;

      

            if (opcija.Equals(EOpcija.IZMENA))
            {
                TxtSifraAK.IsEnabled = false;
            }

        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (opcija.Equals(EOpcija.DODAVANJE) && !ValidacijaSifra(avioK.Sifra))
            {
                avioK.SacuvajAvioKompaniju();
            }
            
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private bool ValidacijaSifra(String sifra)
        {
            foreach (AvioKompanija avio in Data.Instance.AvioKompanije)
            {
                if (avio.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
