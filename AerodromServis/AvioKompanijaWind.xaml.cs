﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AvioKompanija.xaml
    /// </summary>
    public partial class AvioKompanijaWind : Window
    {

        ICollectionView view;

        public AvioKompanijaWind()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Data.Instance.AvioKompanije);
            DGAvioK.ItemsSource = view;
            DGAvioK.IsSynchronizedWithCurrentItem = true;

            DGAvioK.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);


        }


        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AvioKompanijaAddEdit aka = new AvioKompanijaAddEdit(new AvioKompanija());
            aka.ShowDialog();
        }
        private void btnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            AvioKompanija selektovaniAK = view.CurrentItem as AvioKompanija;
            if(selektovaniAK != null)
            {
                AvioKompanija stari = (AvioKompanija) selektovaniAK.Clone();
                AvioKompanijaAddEdit ake = new AvioKompanijaAddEdit(selektovaniAK, AvioKompanijaAddEdit.EOpcija.IZMENA);
                if(ake.ShowDialog() != true)
                {
                    int index = Data.Instance.AvioKompanije.IndexOf(selektovaniAK);
                    Data.Instance.AvioKompanije[index] = stari;
                }
                else
                {
                    selektovaniAK.IzmenaAvioKompanije();
                }
            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan aerodrom");
            }

        }

        private void btnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
              MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                AvioKompanija selektovaniAK = view.CurrentItem as AvioKompanija;
                // Data.Instance.AvioKompanije.Remove(selektovaniAK);
                selektovaniAK.BrisanjeAvioKompanije();

            }
        }

        
    }
}
