﻿using System;
using AerodromWPF.Model;
using AerodromWPF.Database;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AvionDodIzme.xaml
    /// </summary>
    public partial class AvionDodIzme : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Avion avion;
        
        public AvionDodIzme(Avion avion, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();

            this.avion = avion;
            this.opcija = opcija;

            this.DataContext = avion;

            List<string> sifraLeta = new List<string>();

            foreach(Let sifre in Data.Instance.Letovi)
            {
                string sifra = sifre.Sifra;
                sifraLeta.Add(sifra);
            }

            cSifraL.ItemsSource = sifraLeta;
            cAvioK.ItemsSource = Data.Instance.AvioKompanije;

            if (opcija.Equals(EOpcija.IZMENA))
            {
                cSifraL.IsEnabled = false;
                cAvioK.IsEnabled = false;
                TxtEklasa.IsEnabled = false;
                TxtBklasa.IsEnabled = false;
            }


        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(EOpcija.DODAVANJE) && !ValidacijaSifra(avion.BrojLeta))
            {
                avion.SacuvajAvion();
            }
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private bool ValidacijaSifra(String sifra)
        {
            foreach (Avion avion in Data.Instance.Avioni)
            {
                if (avion.BrojLeta.Equals(sifra))
                {
                    MessageBox.Show("Vec postoji avion sa tim letom");
                    return true;
                }
            }
            return false;
        }
    }
}
