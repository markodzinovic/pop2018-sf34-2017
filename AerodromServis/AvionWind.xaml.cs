﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AvionWind.xaml
    /// </summary>
    public partial class AvionWind : Window
    {
        ICollectionView view;
        public AvionWind()
        {
            InitializeComponent();

            

            DGAvioni.ItemsSource = Data.Instance.Avioni;
            DGAvioni.IsSynchronizedWithCurrentItem = true;

            DGAvioni.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view = CollectionViewSource.GetDefaultView(Data.Instance.Avioni);
            view.Filter = CustomFilter;
        }
        private bool CustomFilter(Object obj)
        {
            Avion avion = obj as Avion;
            if (txtPretraga.Equals(String.Empty))
            {
                return avion.Aktivan;
            }
            else
            {
                if (avion.BrojLeta.Contains(txtPretraga.Text))
                {
                    return avion.Aktivan && avion.BrojLeta.Contains(txtPretraga.Text);
                }
                else
                {
                    return avion.Aktivan && avion.AvioKompanija.Contains(txtPretraga.Text);
                }
            }

        }
        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AvionDodIzme ad = new AvionDodIzme(new Avion());
            ad.ShowDialog();
        }

        private void btnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            Avion selektovanAvion = (Avion)DGAvioni.SelectedItem;
            if (selektovanAvion != null)
            {
                Avion stari = (Avion)selektovanAvion.Clone();
                AvionDodIzme ai = new AvionDodIzme(selektovanAvion, AvionDodIzme.EOpcija.IZMENA);
                if (ai.ShowDialog() != true)
                {
                    int indeks = IndeksAvion(selektovanAvion.BrojLeta.ToString());
                    Data.Instance.Avioni[indeks] = stari;
                }
                else
                {
                    selektovanAvion.IzmenaAviona();
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan aerodrom");
            }
            DGAvioni.Items.Refresh();
        }

        private void btnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = (Avion)DGAvioni.SelectedItem;
            if (SelektovanAvion(avion) && avion.Aktivan) // && aerodrom.Aktivan je visak ali kao
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    //     int indeks = IndeksAvion(avion.BrojLeta.ToString());
                    //   Data.Instance.Avioni[indeks].Aktivan = false;
                    //                    Avion selektovanAvion = (Avion)DGAvioni.SelectedItem;
                    //                  Data.Instance.Avioni.Remove(selektovanAvion);
                    avion.BrisanjeAviona();
                }
            }
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksAvion(String brojleta)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Avioni.Count; i++)
            {
                if (Data.Instance.Avioni[i].BrojLeta.Equals(brojleta))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAvion(Avion avion)
        {
            if (avion == null)
            {
                MessageBox.Show("Nije selektovan avion! ");
                return false;
            }
            return true;
        }

        
    }
}
