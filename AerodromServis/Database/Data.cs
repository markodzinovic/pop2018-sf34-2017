﻿using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data.SqlClient;
using System.Data;

namespace AerodromWPF.Database
{
    class Data
    {
        public ObservableCollection<AvioKompanija> AvioKompanije { get; set; }
        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Avion> Avioni { get; set; }
        public ObservableCollection<Karta> Karte { get; set; }


        public string connectionString
        {
            get
            {
                return CONNECTION_STRING;
            }
        }

        public const String CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=AerodromWPF_Baza;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private Data()
        {

            Korisnici = new ObservableCollection<Korisnik>();
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Avioni = new ObservableCollection<Avion>();
            AvioKompanije = new ObservableCollection<AvioKompanija>();
            Karte = new ObservableCollection<Karta>();
            UcitajSveKorisnike();
            UcitajSveAerodrome();
            UcitajSveLetove();
            UcitajSveAvioKomp();
            UcitajSveAvione();
            UcitajSveKarte();
        }

        private static Data _instance = null;

        public static Data Instance
        {

            get
            {
                if (_instance == null)
                    _instance = new Data();
                return _instance;
            }
        }

        public void UcitajSveAerodrome()
        {
            Aerodromi.Clear();
            using(SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Aerodromi";

                SqlDataAdapter daAerodromi = new SqlDataAdapter();
                daAerodromi.SelectCommand = command;

                DataSet dsAerodromi = new DataSet();
                daAerodromi.Fill(dsAerodromi, "Aerodromi");

                foreach (DataRow row in dsAerodromi.Tables["Aerodromi"].Rows)
                {
                    Aerodrom aerodrom = new Aerodrom();

                    aerodrom.Id = (int)row["Id"];
                    aerodrom.Sifra = (string)row["Sifra_Aer"];
                    aerodrom.Naziv = (string)row["Naziv"];
                    aerodrom.Grad = (string)row["Grad"];
                    aerodrom.Aktivan = (bool)row["Aktivan"];
                    

                    Aerodromi.Add(aerodrom);
                }
            }
        }
        public void UcitajSveLetove()
        {
            Letovi.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM LetoviB";

                SqlDataAdapter daLetoviB = new SqlDataAdapter();
                daLetoviB.SelectCommand = command;

                DataSet dsLetoviB = new DataSet();
                daLetoviB.Fill(dsLetoviB, "LetoviB");

                foreach (DataRow row in dsLetoviB.Tables["LetoviB"].Rows)
                {
                    Let let = new Let();

                    let.Id = (int)row["Id"];
                    let.Sifra = (string)row["Sifra_Let"];
                    let.Odrediste = (string)row["Odrediste"];
                    let.Destinacija = (string)row["Destinacija"];
                    let.VremePolaska = (DateTime)row["Vreme_Polaska"];
                    let.VremeDolaska = (DateTime)row["Vreme_Dolaska"];
                    let.Cena = (decimal)row["Cena"];
                    let.NazivAvioK = (string)row["NazivAvioK"];
                    let.Aktivan = (bool)row["Aktivan"];

                    Letovi.Add(let);
                }
            }
        }

        
        public void UcitajSveKorisnike()
        {
            Korisnici.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM KorisniciB";

                SqlDataAdapter daAvioKompa = new SqlDataAdapter();
                daAvioKompa.SelectCommand = command;

                DataSet dsAevioKompa = new DataSet();
                daAvioKompa.Fill(dsAevioKompa, "KorisniciB");

                foreach (DataRow row in dsAevioKompa.Tables["KorisniciB"].Rows)
                {
                    Korisnik Kor = new Korisnik();

                    Kor.Id = (int)row["Id"];
                    Kor.Ime = (string)row["Ime"];
                    Kor.Prezime = (string)row["Prezime"];
                    Kor.KorisnickoIme = (string)row["KorisnickoIme"];
                    Kor.Lozinka = (string)row["Sifra"];
                    Kor.Adresa = (string)row["Adresa"];
                    Kor.Pol = (Korisnik.EPol)Enum.Parse(typeof(Korisnik.EPol), row["Pol"].ToString());
                    Kor.Email = (string)row["E_mail"];
                    Kor.TipK = (Korisnik.ETipKorisnika)Enum.Parse(typeof(Korisnik.ETipKorisnika), row["TipKoris"].ToString());
                    Kor.Aktivan = (bool)row["Aktivan"];

                    Korisnici.Add(Kor);
                }
            }
           
        }

        public void UcitajSveAvioKomp()
        {
            AvioKompanije.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM AvioKompanijeB";

                SqlDataAdapter daAvioKompa = new SqlDataAdapter();
                daAvioKompa.SelectCommand = command;

                DataSet dsAevioKompa = new DataSet();
                daAvioKompa.Fill(dsAevioKompa, "AvioKompanijeB");

                foreach (DataRow row in dsAevioKompa.Tables["AvioKompanijeB"].Rows)
                {
                    AvioKompanija avioK = new AvioKompanija();

                    avioK.Id = (int)row["Id"];
                    avioK.Sifra = (string)row["Sifra_Kom"];
                    avioK.NazivAvioK = (string)row["NazivAvioK"];
                    avioK.Aktivan = (bool)row["Aktivan"];

                    AvioKompanije.Add(avioK);
                }
            }

        }

        public void UcitajSveAvione()
        {
            Avioni.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM AvioniB";

                SqlDataAdapter daAvioni = new SqlDataAdapter();
                daAvioni.SelectCommand = command;

                DataSet dsAvioni = new DataSet();
                daAvioni.Fill(dsAvioni, "AvioniB");

                foreach (DataRow row in dsAvioni.Tables["AvioniB"].Rows)
                {
                    Avion avion = new Avion();

                    avion.Id = (int)row["Id"];
                    avion.Pilot = (string)row["Pilot"];
                    avion.BrojLeta = (string)row["Sifra_Let"];
                    avion.SedistaEkonomskeKlase = (int)row["Sed_Ekon"];
                    avion.SedistaBiznisKlase = (int)row["Sed_Bizn"];
                    avion.AvioKompanija = (string)row["NazivAvioK"];
                    avion.Aktivan = (bool)row["Aktivan"];


                    Avioni.Add(avion);
                }
            }
        }

        public void UcitajSveKarte()
        {
            Karte.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Karte";

                SqlDataAdapter dKarte = new SqlDataAdapter();
                dKarte.SelectCommand = command;

                DataSet dsKarte = new DataSet();
                dKarte.Fill(dsKarte, "Karte");

                foreach (DataRow row in dsKarte.Tables["Karte"].Rows)
                {
                    Karta karta = new Karta();

                    karta.Id = (int)row["Id"];
                    karta.SifraLeta = (string)row["SifraLeta"];
                    karta.BrojSedista = (string)row["BrojSedista"];
                    karta.Putnik = (string)row["Putnik"];
                    karta.KlasaSedista = (string)row["KlasaSedista"];
                    karta.Cena = (decimal)row["Cena"];
                    karta.Aktivan = (bool)row["Aktivan"];

                    Karte.Add(karta);
                }
            }
        }

    }
}
