﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.Database;
using AerodromWPF.Model;
using AerodromWPF.KorisnikWPF;
using System.Collections.ObjectModel;

namespace AerodromWPF.GlavniWinWPF
{
    /// <summary>
    /// Interaction logic for Glavni.xaml
    /// </summary>
    public partial class Glavni : Window
    {
        public Korisnik korisnikUlog;
        public ObservableCollection<Let> pretragaLet { get; set; }

        public Glavni()
        {
            InitializeComponent();
            cOdredistePro.ItemsSource = Data.Instance.Aerodromi;
            cDestinacijaPr.ItemsSource = Data.Instance.Aerodromi;

            rbEkonomska.IsChecked = true;

        }

        private void btnPrijava_Click(object sender, RoutedEventArgs e)
        {
            var provera = 0;
            foreach(Korisnik kor in Data.Instance.Korisnici)
            {
                if(txtKorisnickoIme.Text.Equals(kor.KorisnickoIme) && pswdSifra.Password.Equals(kor.Lozinka))
                {
                    provera = 1;
                    var korisnikUlog = kor;
                    if (korisnikUlog.TipK.Equals(Korisnik.ETipKorisnika.KORISNIK))
                    {
                        GlavniKor gk = new GlavniKor(korisnikUlog);
                        gk.ShowDialog();
                        return;

                    }else if (kor.TipK.Equals(Korisnik.ETipKorisnika.ADMINISTRATOR))
                    {
                        MainWindow mw = new MainWindow(korisnikUlog);
                        mw.ShowDialog();
                        return;

                    }

                }
            }
            if(provera == 0)
            {
                MessageBox.Show("Niste dobro uneli podatke");
            }
            
        }

        private void btnRegistracija_Click(object sender, RoutedEventArgs e)
        {
            Registracija kd = new Registracija(new Korisnik(),Registracija.Opcija.REGISTRACIJA);
            kd.ShowDialog();
        }

        private void btnPretraga_Click(object sender, RoutedEventArgs e)
        {
            listLetovi.Items.Clear();

            try
            {
                var provera = 0;
                foreach (Let let in Data.Instance.Letovi)
                {
                    if (cOdredistePro.Text.ToString().Equals(let.Odrediste) && cDestinacijaPr.Text.Equals(let.Destinacija))
                    {
                        provera = 1;

                        listLetovi.Items.Add(let);

                        



                    }


                }
                if (provera == 0)
                {
                    MessageBox.Show("Za odabrano odrediste i destinaciju ne postoji let");
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

            }



        }
    }
}
