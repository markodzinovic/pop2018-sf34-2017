﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.GlavniWinWPF
{
    /// <summary>
    /// Interaction logic for Registracija.xaml
    /// </summary>
    public partial class Registracija : Window
    {
        public enum Opcija { REGISTRACIJA };
        public Korisnik korisnik;
        public Opcija opcija;
        public Registracija(Korisnik korisnik, Opcija opcija = Opcija.REGISTRACIJA)
        {
            InitializeComponent();

            this.korisnik = korisnik;
            this.opcija = opcija;

            this.DataContext = korisnik;

            cPol.Items.Add(Korisnik.EPol.MUSKI);
            cPol.Items.Add(Korisnik.EPol.ZENSKI);

            if (opcija.Equals(Opcija.REGISTRACIJA))
            {
                cTipKorsinika.Items.Add(Korisnik.ETipKorisnika.KORISNIK);
                cTipKorsinika.IsEnabled = false;
            }
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(Opcija.REGISTRACIJA) && !ValidacijaKorisnickoIme(korisnik.KorisnickoIme))
            {
                korisnik.SacuvajKorisnika();
            }

        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }

        private bool ValidacijaKorisnickoIme(String korisnickoIme)
        {
            foreach (Korisnik korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(korisnickoIme))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
