﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for KartaWind.xaml
    /// </summary>
    public partial class KartaWind : Window
    {
        ICollectionView view;
        public KartaWind()
        {
            InitializeComponent();

            DGKarte.ItemsSource = Data.Instance.Karte;
            DGKarte.IsSynchronizedWithCurrentItem = true;

            view = CollectionViewSource.GetDefaultView(Data.Instance.Karte);
            view.Filter = CustomFilter;

            DGKarte.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private bool CustomFilter(Object obj)
        {
            Karta karat = obj as Karta;
            if (txtPretraga.Equals(String.Empty))
            {
                return karat.Aktivan;
            }
            else
            {
                if (karat.SifraLeta.Contains(txtPretraga.Text))
                {
                    return karat.Aktivan && karat.SifraLeta.Contains(txtPretraga.Text);
                }
                else if (karat.Putnik.Contains(txtPretraga.Text))
                {
                    return karat.Aktivan && karat.Putnik.Contains(txtPretraga.Text);
                }
                else
                {
                    return karat.Aktivan && karat.KlasaSedista.Contains(txtPretraga.Text);
                }
            }

        }

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            LetoviKarta lk = new LetoviKarta();
            lk.ShowDialog();

        }
        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
