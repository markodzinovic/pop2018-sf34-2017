﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for KarteADDEDIt.xaml
    /// </summary>
    public partial class KarteADDEDIt : Window
    {
        private Let let;
        public Karta karta;
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        public KarteADDEDIt(Karta karta, Let let, Boolean klasaSed, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.opcija = opcija;

            this.karta = karta;
            this.let = let;

            cKorisnickoIme.ItemsSource = Data.Instance.Korisnici;
            txtSifraLeta.Text = let.Sifra;
            txtOdred.Text = let.Odrediste;
            txtDest.Text = let.Destinacija;
            txtVremeDola.Text = let.VremeDolaska.ToString();
            txtVremePola.Text = let.VremePolaska.ToString();

            List<string> sedista = new List<string>();
            if (klasaSed)
            {
                var biznisCena = let.Cena + let.Cena / 2;
                txtCena.Text = biznisCena.ToString();
                txtKlasa.Text = "BIZNIS";

                foreach (Avion sediste in Data.Instance.Avioni)
                {
                    if (sediste.BrojLeta.Equals(let.Sifra))
                    {
                        string[] ekono = { "A", "B", "C" };

                        for (int j = 0; j < ekono.Count(); j++)
                        {
                            for (int i = 1; i <= sediste.SedistaBiznisKlase; i++)
                            {
                                string oznaka_sedista = ekono[j] + i.ToString();
                                if (ValidacijaSedista(oznaka_sedista, sediste.BrojLeta, txtKlasa.Text))
                                {
                                    continue;
                                }
                                else
                                {
                                    sedista.Add(oznaka_sedista);
                                }
                            }
                        }
                    }
                    Sedista.ItemsSource = sedista;

                }
            }
            if (!klasaSed)
            {
                txtCena.Text = let.Cena.ToString();
                txtKlasa.Text = "EKONOMSKA";

                foreach (Avion sediste in Data.Instance.Avioni)
                {
                    if (sediste.BrojLeta.Equals(let.Sifra))
                    {
                        string[] ekono = { "A", "B", "C" };

                        for (int j = 0; j < ekono.Count(); j++)
                        {
                            for (int i = 1; i <= sediste.SedistaEkonomskeKlase; i++)
                            {
                                string oznaka_sedista = ekono[j] + i.ToString();
                                if (ValidacijaSedista(oznaka_sedista, sediste.BrojLeta, txtKlasa.Text))
                                {
                                    continue;
                                }
                                else
                                {
                                    sedista.Add(oznaka_sedista);
                                }
                            }
                        }
                    }
                    Sedista.ItemsSource = sedista;

                }
            }
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = true;
            if(validacija() == true && opcija.Equals(EOpcija.DODAVANJE))
            {
                karta.BrojSedista = Sedista.Text;
                karta.Cena = decimal.Parse(txtCena.Text);
                karta.Putnik = cKorisnickoIme.SelectedItem.ToString();
                karta.KlasaSedista = txtKlasa.Text;
                karta.SifraLeta = txtSifraLeta.Text;

                karta.SacuvajKartu();
            }
            
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private bool ValidacijaSedista(String sed, String sifraLeta, String klasa)
        {
            foreach (Karta karatSed in Data.Instance.Karte)
            {
                if (karatSed.BrojSedista.Equals(sed) && karatSed.SifraLeta.Equals(sifraLeta) && karatSed.KlasaSedista.Equals(klasa))
                {
                    return true;
                }
            }
            return false;
        }
        public bool validacija()
        {
            if (cKorisnickoIme.Text.Equals(""))
            {
                MessageBox.Show("Nije dodat putnik");
                return false;
            }
            if (Sedista.Text.Equals(""))
            {
                MessageBox.Show("Nije izabrano sediste");
                return false;
            }
            return true;
        }
    }
}
