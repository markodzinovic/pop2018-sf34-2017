﻿using AerodromWPF.Model;
using AerodromWPF.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for KorisnikDodIzm.xaml
    /// </summary>
    public partial class KorisnikDodIzm : Window
    {   
        public enum Opcija { DODAVANJE , IZMENA, REGISTRACIJA};
        public Korisnik korisnik;
        public Opcija opcija;
        public KorisnikDodIzm(Korisnik korisnik, Opcija opcija = Opcija.DODAVANJE)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.opcija = opcija;

            this.DataContext = korisnik;

            cPol.Items.Add(Korisnik.EPol.MUSKI);
            cPol.Items.Add(Korisnik.EPol.ZENSKI);

           
            cTipKorsinika.Items.Add(Korisnik.ETipKorisnika.KORISNIK);
            cTipKorsinika.Items.Add(Korisnik.ETipKorisnika.ADMINISTRATOR);
                
            

            


            if (opcija.Equals(Opcija.IZMENA))
            {
                TxtKorisnickoIme.IsEnabled = false;
            }

        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(Opcija.DODAVANJE) && !ValidacijaKorisnickoIme(korisnik.KorisnickoIme))
            {
                korisnik.SacuvajKorisnika();
            }
            else
            {
                korisnik.IzmenaKorisnika();
            }
           

        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    

    private bool ValidacijaKorisnickoIme(String korisnickoIme)
    {
        foreach (Korisnik korisnik in Data.Instance.Korisnici)
        {
            if (korisnik.KorisnickoIme.Equals(korisnickoIme))
            {
                return true;
            }
        }
        return false;
    }

    }
}
