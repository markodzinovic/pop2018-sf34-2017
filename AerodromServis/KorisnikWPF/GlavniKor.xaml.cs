﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.Model;
using AerodromWPF.Database;
using System.Collections.ObjectModel;

namespace AerodromWPF.KorisnikWPF
{
    /// <summary>
    /// Interaction logic for GlavniKor.xaml
    /// </summary>
    public partial class GlavniKor : Window
    {
        public ObservableCollection<Let> pretragaLet{ get; set; }
        public Korisnik korisnik;

        public GlavniKor(Korisnik korUl)
        {
            InitializeComponent();
            foreach (Korisnik ko in Data.Instance.Korisnici)
            {
                if (ko.KorisnickoIme.Equals(korUl.KorisnickoIme))
                {
                    this.korisnik = ko;
                    txtUlogovaniK.Text = ko.KorisnickoIme;
                    
                }
            }
                        

            
                listLetovi.Visibility = Visibility.Hidden;
                btnRezervisi.Visibility = Visibility.Hidden;

            cOdredistePro.ItemsSource = Data.Instance.Aerodromi;
            cDestinacijaPr.ItemsSource = Data.Instance.Aerodromi;

            rbEkonomska.IsChecked = true;



        }

        private void btnPregledProfila_Click(object sender, RoutedEventArgs e)
        {
             Profil pr = new Profil(korisnik);
                    pr.ShowDialog();
            
        }

        private void btnMojeKart_Click(object sender, RoutedEventArgs e)
        {
            MojeKarte mk = new MojeKarte(korisnik);
            mk.ShowDialog();
        }

        private void btnTrenutniLetovi_Click(object sender, RoutedEventArgs e)
        {
            
                    TrenutniLetov tl = new TrenutniLetov(korisnik);
                    tl.ShowDialog();
             
        }

        private void btnPretraga_Click(object sender, RoutedEventArgs e)
        {
            listLetovi.Items.Clear();
            
            try
            {
                var provera = 0;
                foreach (Let let in Data.Instance.Letovi)
                {
                    if( cOdredistePro.Text.ToString().Equals(let.Odrediste) && cDestinacijaPr.Text.Equals(let.Destinacija))
                    {
                        provera = 1;
                        
                        listLetovi.Items.Add(let);
                    
                        listLetovi.Visibility = Visibility.Visible;
                        
                        btnRezervisi.Visibility = Visibility.Visible;


                        
                    }


                }if(provera==0)
                    {
                    MessageBox.Show("Za odabrano odrediste i destinaciju ne postoji let");
                }


            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                
            }
            


        }

        private void btnOdjava_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnRezervisi_Click(object sender, RoutedEventArgs e)
        {
            
                if (listLetovi.SelectedItem == null)
                {

                    MessageBox.Show("ssss");
                this.DialogResult = false;
                return;

                }
            else
            {
                KupiKartuKor kk = new KupiKartuKor(new Karta(), (Let)listLetovi.SelectedItem, korisnik, (Boolean)rbBiznis.IsChecked);
                kk.ShowDialog();
                return;
            }
            
        }
    }
}
