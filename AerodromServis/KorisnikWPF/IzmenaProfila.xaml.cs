﻿using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.KorisnikWPF
{
    /// <summary>
    /// Interaction logic for IzmenaProfila.xaml
    /// </summary>
    public partial class IzmenaProfila : Window
    {
        public Korisnik korisnik;
        public IzmenaProfila(Korisnik korisnik)
        {
            InitializeComponent();
            InitializeComponent();
            this.korisnik = korisnik;
            this.DataContext = korisnik;
            cPol.Items.Add(Korisnik.EPol.MUSKI);
            cPol.Items.Add(Korisnik.EPol.ZENSKI);
            cTipKorsinika.Items.Add(Korisnik.ETipKorisnika.KORISNIK);
            TxtKorisnickoIme.IsEnabled = false;
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            korisnik.IzmenaKorisnika();
            Close();
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        

    }
}
