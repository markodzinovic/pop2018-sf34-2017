﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.Database;
using AerodromWPF.Model;


namespace AerodromWPF.KorisnikWPF
{
    /// <summary>
    /// Interaction logic for Profil.xaml
    /// </summary>
    public partial class Profil : Window
    {
        public Korisnik ulogovan;
        public Profil(Korisnik korUlog)
        {
            InitializeComponent();

            this.ulogovan = korUlog;
            foreach (Korisnik ko in Data.Instance.Korisnici)
            {
                if (ko.KorisnickoIme.Equals(ulogovan.KorisnickoIme))
                {
                    
                    txtkorisnickoIme.Text = ko.KorisnickoIme;
                    txtLozinka.Text = ko.Lozinka;
                    txtIme.Text = ko.Ime;
                    txtPrezime.Text = ko.Prezime;
                    txtTip.Text = ko.TipK.ToString();
                    txtAdresa.Text = ko.Adresa;
                    txtEmail.Text = ko.Email;
                    txtPol.Text = ko.Pol.ToString();
                }
            }
            
        }

        private void btnIzmena_Click(object sender, RoutedEventArgs e)
        {
            IzmenaProfila ip = new IzmenaProfila(ulogovan);
            ip.ShowDialog(); 
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
