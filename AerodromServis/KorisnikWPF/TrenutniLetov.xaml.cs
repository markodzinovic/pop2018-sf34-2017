﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.KorisnikWPF
{
    /// <summary>
    /// Interaction logic for TrenutniLetov.xaml
    /// </summary>
    public partial class TrenutniLetov : Window
    {
        public TrenutniLetov(Korisnik korisnik)
        {
            InitializeComponent();

            ObservableCollection<Let> imajuAvion = new ObservableCollection<Let>();

            foreach(Avion a in Data.Instance.Avioni)
            {
                foreach(Let l in Data.Instance.Letovi)
                {
                    if (a.BrojLeta.Equals(l.Sifra))
                    {
                        imajuAvion.Add(l);
                    }
                }
            }

            DGLetovi.ItemsSource = imajuAvion;
            DGLetovi.IsSynchronizedWithCurrentItem = true;

            txtKorisnickoIme.Text = korisnik.KorisnickoIme;

            rbEkonomska.IsChecked = true;
            


            DGLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnKupiKartu_Click(object sender, RoutedEventArgs e)
        {

            Let Selektovanlet = (Let)DGLetovi.SelectedItem;
            if (Selektovanlet != null)
            {
                Let stari = (Let)Selektovanlet.Clone();
                foreach (Korisnik kor in Data.Instance.Korisnici)
                {
                    if (txtKorisnickoIme.Text.Equals(kor.KorisnickoIme))
                    {
                        KupiKartuKor kkk = new KupiKartuKor(new Karta(),stari, kor, (Boolean)rbBiznis.IsChecked );
                        kkk.ShowDialog();

                    }
                }


            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan let");
            }
        }
    }
}
