﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for KorisnikWindow.xaml
    /// </summary>
    public partial class KorisnikWindow : Window
    {
        ICollectionView view;
        public KorisnikWindow()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Data.Instance.Korisnici);
            view.Filter = CustomFilter;

            DGKorisnik.ItemsSource = Data.Instance.Korisnici;
            DGKorisnik.IsSynchronizedWithCurrentItem = true;

            DGKorisnik.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }
        private bool CustomFilter(Object obj)
        {
            Korisnik korisnik = obj as Korisnik;
            if (txtPretraga.Equals(String.Empty))
            {
                return korisnik.Aktivan;
            }
            else
            {
                if (korisnik.Ime.Contains(txtPretraga.Text))
                {
                    return korisnik.Aktivan && korisnik.Ime.Contains(txtPretraga.Text);
                }
                else if (korisnik.Prezime.Contains(txtPretraga.Text))
                {
                    return korisnik.Aktivan && korisnik.Prezime.Contains(txtPretraga.Text);
                }
                else if (korisnik.KorisnickoIme.Contains(txtPretraga.Text))
                {
                    return korisnik.Aktivan && korisnik.KorisnickoIme.Contains(txtPretraga.Text); 
                }
                else 
                {
                    return korisnik.Aktivan && korisnik.Pol.ToString().Contains(txtPretraga.Text);
                }
            }
        }

    private void BtnDodajK_Click(object sender, RoutedEventArgs e)
        {
            KorisnikDodIzm kd = new KorisnikDodIzm(new Korisnik());
            kd.ShowDialog();

        }

        private void BtnIzmeniK_Click(object sender, RoutedEventArgs e)
        {
            Korisnik SelektovanKorisnik = (Korisnik)DGKorisnik.SelectedItem;
            if (SelektovanKorisnik != null)
            {
                Korisnik stari = (Korisnik)SelektovanKorisnik.Clone();
                KorisnikDodIzm ki = new KorisnikDodIzm(SelektovanKorisnik, KorisnikDodIzm.Opcija.IZMENA);
                if (ki.ShowDialog() != true)
                {
                    int indeks = IndeksKorisnik(SelektovanKorisnik.KorisnickoIme);
                    Data.Instance.Korisnici[indeks] = stari;
                }
                

            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan aerodrom");
            }
            DGKorisnik.Items.Refresh();
        }

        private void BtnObrisiK_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DGKorisnik.SelectedItem;
            if (SelektovanKorisnik(korisnik) && korisnik.Aktivan) // && aerodrom.Aktivan je visak ali kao
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    //                    int indeks = IndeksKorisnik(korisnik.KorisnickoIme);
                    //                    Data.Instance.Aerodromi[indeks].Aktivan = false;
                    //                   Korisnik selektovan = (Korisnik)DGKorisnik.SelectedItem;
                    //                 Data.Instance.Korisnici.Remove(selektovan);
                    korisnik.BrisanjeKorisnika();
                }
            }

        }

        private void BtnNazadK_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksKorisnik(String korisnickoIme)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Korisnici.Count; i++)
            {
                if (Data.Instance.Korisnici[i].KorisnickoIme.Equals(korisnickoIme))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanKorisnik(Korisnik korisnik)
        {
            if (korisnik == null)
            {
                MessageBox.Show("Nije selektovan korisnik! ");
                return false;
            }
            return true;
        }

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
