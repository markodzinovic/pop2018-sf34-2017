﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for Letovi.xaml
    /// </summary>
    public partial class Letovi : Window
    {
        ICollectionView view;
        public Letovi()
        {
            InitializeComponent();

            view = CollectionViewSource.GetDefaultView(Data.Instance.Letovi);
            view.Filter = CustomFilter;

            DGLetovi.ItemsSource = Data.Instance.Letovi;
            DGLetovi.IsSynchronizedWithCurrentItem = true;

            DGLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);


        }
        private bool CustomFilter(Object obj)
        {
            Let let = obj as Let;
            if (txtPretraga.Equals(String.Empty))
            {
                return let.Aktivan;
            }
            else
            {
                if (let.Sifra.Contains(txtPretraga.Text))
                {
                    return let.Aktivan && let.Sifra.Contains(txtPretraga.Text);
                }
                else if (let.NazivAvioK.Contains(txtPretraga.Text))
                {
                    return let.Aktivan && let.NazivAvioK.Contains(txtPretraga.Text);
                }
                else if (let.Odrediste.Contains(txtPretraga.Text))
                {
                    return let.Aktivan && let.Odrediste.Contains(txtPretraga.Text);
                }
                else if (let.Destinacija.Contains(txtPretraga.Text))
                {
                    return let.Aktivan && let.Destinacija.Contains(txtPretraga.Text);
                }
                else if (let.VremeDolaska.ToString().Contains(txtPretraga.Text))
                {
                    return let.Aktivan && let.VremeDolaska.ToString().Contains(txtPretraga.Text);
                }
                else if(let.VremePolaska.ToString().Contains(txtPretraga.Text))
                {
                    return let.Aktivan && let.VremePolaska.ToString().Contains(txtPretraga.Text);
                }
                else
                {
                    return let.Aktivan && let.Cena.ToString().Equals(txtPretraga.Text);
                }
            }
        
        }

        private void BtnDodajLet_Click(object sender, RoutedEventArgs e)
        {
            LetoviDodIzm ld = new LetoviDodIzm(new Let());
            ld.ShowDialog();
        }

        private void BtnIzmeniLet_Click(object sender, RoutedEventArgs e)
        {
            Let Selektovanlet = (Let)DGLetovi.SelectedItem;
            if (Selektovanlet != null)
            {
                Let stari = (Let)Selektovanlet.Clone();
                LetoviDodIzm li = new LetoviDodIzm(Selektovanlet, LetoviDodIzm.Opcija.IZMENA);
                if (li.ShowDialog() != true)
                {
                    int indeks = IndeksLet(Selektovanlet.Sifra);
                    Data.Instance.Letovi[indeks] = stari;
                }
                else
                {
                    Selektovanlet.IzmenaLetova();
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan ni jedan aerodrom");
            }
            DGLetovi.Items.Refresh();
        }

        private void BtnObrisiLet_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DGLetovi.SelectedItem;
            if (SelektovanLet(let) && let.Aktivan) // && aerodrom.Aktivan je visak ali kao
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    //                   int indeks = IndeksLet(let.Sifra);
                    //                   Data.Instance.Letovi[indeks].Aktivan = false;
                    //                   Let selektovan = (Let)DGLetovi.SelectedItem;
                    //                  Data.Instance.Letovi.Remove(selektovan);
                    let.BrisanjeLetova();
                }
            }

        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksLet(String sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Letovi.Count; i++)
            {
                if (Data.Instance.Letovi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Nije selektovan let! ");
                return false;
            }
            return true;
        }

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }
}
