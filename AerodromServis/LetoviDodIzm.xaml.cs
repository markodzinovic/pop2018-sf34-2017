﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for LetoviDodIzm.xaml
    /// </summary>
    public partial class LetoviDodIzm : Window
    {
        public enum Opcija { DODAVANJE, IZMENA };
        private Let let;
        private Opcija opcija;
        public LetoviDodIzm(Let let, Opcija opcija = Opcija.DODAVANJE)
        {
            InitializeComponent();
            this.let = let;
            this.opcija = opcija;

            this.DataContext = let;

            cOdrediste.ItemsSource = Data.Instance.Aerodromi;
            cDestinacija.ItemsSource = Data.Instance.Aerodromi;
            cAvioK.ItemsSource = Data.Instance.AvioKompanije;

            if (opcija.Equals(Opcija.IZMENA))
            {
                TxtSifra.IsEnabled = false;
            }


        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(Opcija.DODAVANJE) && !ValidacijaLetSifra(let.Sifra))
            {
                let.SacuvajLet();
            }
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private bool ValidacijaLetSifra(String sifra)
        {
            foreach (Let let in Data.Instance.Letovi)
            {
                if (let.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
