﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for LetoviKarta.xaml
    /// </summary>
    public partial class LetoviKarta : Window
    {
        public LetoviKarta()
        {
            InitializeComponent();

            ObservableCollection<Let> imajuAvion = new ObservableCollection<Let>();

            foreach (Avion a in Data.Instance.Avioni)
            {
                foreach (Let l in Data.Instance.Letovi)
                {
                    if (a.BrojLeta.Equals(l.Sifra))
                    {
                        imajuAvion.Add(l);
                    }
                }
            }
            DGLetovi.ItemsSource = imajuAvion;
            DGLetovi.IsSynchronizedWithCurrentItem = true;
            rbEkonomska.IsChecked = true;
            DGLetovi.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void btnKupiKartu_Click(object sender, RoutedEventArgs e)
        {
            Let Selektovanlet = (Let)DGLetovi.SelectedItem;
            if (Selektovanlet != null)
            {
                Let stari = (Let)Selektovanlet.Clone();

                KarteADDEDIt kadd = new KarteADDEDIt(new Karta(),stari, (Boolean) rbBiznis.IsChecked, KarteADDEDIt.EOpcija.DODAVANJE);
                kadd.ShowDialog();
            }
        }

        private void btnNazad_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
