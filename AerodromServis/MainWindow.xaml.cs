﻿using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(Korisnik korisUlog)
        {
            InitializeComponent();
            txtkorisnickoIme.Text = korisUlog.KorisnickoIme;
        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            AerodromiWindow aerodromiWindow = new AerodromiWindow();
            aerodromiWindow.Show();

        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            Letovi lt = new Letovi();
            lt.Show();
        }



        private void btnKorisnici_Click_1(object sender, RoutedEventArgs e)
        {
            KorisnikWindow kw = new KorisnikWindow();
            kw.Show();
        }

        private void btnAvioni_Click(object sender, RoutedEventArgs e)
        {
            AvionWind aw = new AvionWind();
            aw.ShowDialog();
        }

        private void btnAvioK_Click(object sender, RoutedEventArgs e)
        {
            AvioKompanijaWind ak = new AvioKompanijaWind();
            ak.ShowDialog();
        }

        private void btnOdjava_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnKarte_Click(object sender, RoutedEventArgs e)
        {
            KartaWind kw = new KartaWind();
            kw.ShowDialog();
        }
    }
}
