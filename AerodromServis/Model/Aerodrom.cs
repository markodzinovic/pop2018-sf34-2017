﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AerodromWPF.Model
{
    public class Aerodrom : INotifyPropertyChanged, ICloneable
    {
        public Aerodrom ()
        {
            aktivan = true;
        }

        private String sifra;

        public String Sifra
        {
            get { return sifra; }
            set { sifra = value; OnProperyChanged("Sifra"); }
        }

        private String naziv;

        public String Naziv
        {
            get { return naziv; }
            set { naziv = value; OnProperyChanged("Naziv"); }
        }



        private String grad;

        public String Grad
        {
            get { return grad; }
            set { grad = value; OnProperyChanged("Grad");}
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("Id"); }
        }

        private bool aktivan;



        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"{Sifra}";
        }


        public object Clone()
        {
            Aerodrom aerodrom = new Aerodrom()
            {
                Sifra = this.Sifra,
                Grad = this.Grad,
                Naziv = this.Naziv,
                Aktivan = this.Aktivan,
                Id = this.Id
            };
            return aerodrom;
        }

        public void SacuvajAerodrom()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Aerodromi(Sifra_Aer,Naziv,Grad,Aktivan)" + "VALUES(@Sifra,@Naziv,@Grad,@Aktivan)";

                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Naziv", this.Naziv));
                command.Parameters.Add(new SqlParameter("@Grad", this.Grad));
                command.Parameters.Add(new SqlParameter("@Aktivan", true));

                command.ExecuteNonQuery();
            }
            Database.Data.Instance.UcitajSveAerodrome();
        }
        
        public void IzmenaAerodroma()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update Aerodromi set Sifra_Aer=@Sifra, Naziv=@Naziv, Grad=@Grad, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@Naziv", this.Naziv));
                comm.Parameters.Add(new SqlParameter("@Grad", this.Grad));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));

                comm.ExecuteNonQuery();

            }
            catch(Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveAerodrome();
        }

        public void BrisanjeAerodroma()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update Aerodromi set Sifra_Aer=@Sifra, Naziv=@Naziv, Grad=@Grad, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@Naziv", this.Naziv));
                comm.Parameters.Add(new SqlParameter("@Grad", this.Grad));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan = false));

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveAerodrome();
        }
    }
}
