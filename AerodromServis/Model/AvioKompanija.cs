﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AerodromWPF.Model
{
    public class AvioKompanija : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;


        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public AvioKompanija()
        {
            Aktivan = true;
            
        }

        public object Clone()
        {
            return new AvioKompanija
            {
                Sifra = this.Sifra,
                NazivAvioK = this.NazivAvioK,
                Id = this.Id,
                Aktivan = this.Aktivan
                

            };
        }

        private String sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnProperyChanged("Sifra"); }
        }

        private String nazivAvioK;

        public string NazivAvioK
        {
            get { return nazivAvioK; }
            set { nazivAvioK = value; OnProperyChanged("NazivAvioK"); }
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("Id"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }

        }
        public override string ToString()
        {
            return $"{NazivAvioK}";
        }

        public void SacuvajAvioKompaniju()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO AvioKompanijeB(Sifra_Kom,NazivAvioK,Aktivan)" + "VALUES(@Sifra,@NazivAvioK,@Aktivan)";

                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@NazivAvioK", this.NazivAvioK));
                command.Parameters.Add(new SqlParameter("@Aktivan", true));

                command.ExecuteNonQuery();
            }
            Database.Data.Instance.UcitajSveAvioKomp();
        }

        public void IzmenaAvioKompanije()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update AvioKompanijeB set Sifra_Kom=@Sifra, NazivAvioK=@NazivAvioK, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@NazivAvioK", this.NazivAvioK));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveAvioKomp();
        }
        public void BrisanjeAvioKompanije()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update AvioKompanijeB set Sifra_Kom=@Sifra, NazivAvioK=@NazivAvioK, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@NazivAvioK", this.NazivAvioK));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan = false));

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveAvioKomp();
        }

    }

}
