﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AerodromWPF.Model
{
    public class Avion : INotifyPropertyChanged, ICloneable
    {
        

        public event PropertyChangedEventHandler PropertyChanged;

    private void OnProperyChanged(string name)
    {
        PropertyChangedEventHandler handler = PropertyChanged;
        if (handler != null)
        {
            handler(this, new PropertyChangedEventArgs(name));
        }
    }
        public Avion()
        {
            Aktivan = true;
        }

    private String pilot;

    public string Pilot
    {
        get { return pilot; }
        set { pilot = value; OnProperyChanged("pilot"); }
    }

    private String brojLeta;

    public string BrojLeta
    {
        get { return brojLeta; }
        set { brojLeta = value; OnProperyChanged("BrojLeta"); }
    }

    private int sedistaBiznisKlase;

    public int SedistaBiznisKlase
    {
        get { return sedistaBiznisKlase; }
        set { sedistaBiznisKlase = value; OnProperyChanged("SedistaBiznisKlase"); }
    }

    private int sedistaEkonomskeKlase;

    public int SedistaEkonomskeKlase
    {
        get { return sedistaEkonomskeKlase; }
        set { sedistaEkonomskeKlase = value; OnProperyChanged("SedistaEkonomskeKlase"); }
    }

    private String avioKompanija;

    public string AvioKompanija
    {
        get { return avioKompanija; }
        set { avioKompanija = value; OnProperyChanged("AvioKompanija"); }

    }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("Id"); }
        }


        private bool aktivan;

    public bool Aktivan
    {
        get { return aktivan; }
        set { aktivan = value; }

    }

    public object Clone()
    {
        return new Avion
        {
            Pilot = this.Pilot,
            BrojLeta = this.BrojLeta,
            sedistaEkonomskeKlase = this.SedistaEkonomskeKlase,
            SedistaBiznisKlase = this.SedistaBiznisKlase,
            AvioKompanija = this.AvioKompanija,
            Id = this.Id,
            Aktivan = this.Aktivan
            
        };
    }

        public void SacuvajAvion()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO AvioniB(Pilot,Sifra_Let,Sed_Ekon,Sed_Bizn,NazivAvioK,Aktivan)"
                                    + "VALUES(@Pilot,@BrojLeta,@SedistaEkonomskeKlase,@SedistaBiznisKlase,@AvioKompanija,@Aktivan)";

                command.Parameters.Add(new SqlParameter("@Pilot", this.Pilot));
                command.Parameters.Add(new SqlParameter("@BrojLeta", this.BrojLeta));
                command.Parameters.Add(new SqlParameter("@SedistaEkonomskeKlase", this.SedistaEkonomskeKlase));
                command.Parameters.Add(new SqlParameter("@SedistaBiznisKlase", this.SedistaBiznisKlase));
                command.Parameters.Add(new SqlParameter("@AvioKompanija", this.AvioKompanija));
                command.Parameters.Add(new SqlParameter("@Aktivan", true));

                command.ExecuteNonQuery();
            }
            Database.Data.Instance.UcitajSveAvione();
        }
        public void IzmenaAviona()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update AvioniB set Pilot=@Pilot,Sifra_Let=@BrojLeta,Sed_Ekon=@SedistaEkonomskeKlase,Sed_Bizn=@SedistaBiznisKlase,NazivAvioK=@AvioKompanija, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Pilot", this.Pilot));
                comm.Parameters.Add(new SqlParameter("@BrojLeta", this.BrojLeta));
                comm.Parameters.Add(new SqlParameter("@SedistaEkonomskeKlase", this.SedistaEkonomskeKlase));
                comm.Parameters.Add(new SqlParameter("@SedistaBiznisKlase", this.SedistaBiznisKlase));
                comm.Parameters.Add(new SqlParameter("@AvioKompanija", this.AvioKompanija));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveAvione();
        }
        public void BrisanjeAviona()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update AvioniB set Pilot=@Pilot,Sifra_Let=@BrojLeta,Sed_Ekon=@SedistaEkonomskeKlase,Sed_Bizn=@SedistaBiznisKlase,NazivAvioK=@AvioKompanija, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Pilot", this.Pilot));
                comm.Parameters.Add(new SqlParameter("@BrojLeta", this.BrojLeta));
                comm.Parameters.Add(new SqlParameter("@SedistaEkonomskeKlase", this.SedistaEkonomskeKlase));
                comm.Parameters.Add(new SqlParameter("@SedistaBiznisKlase", this.SedistaBiznisKlase));
                comm.Parameters.Add(new SqlParameter("@AvioKompanija", this.AvioKompanija));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan=false));

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveAvione();
        }
    }

}

