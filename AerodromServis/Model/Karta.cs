﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AerodromWPF.Model
{
        public class Karta : INotifyPropertyChanged, ICloneable
        {

        
        public Karta()
            {
                Aktivan = true;
            }

            private String sifraLeta;

            public string SifraLeta
            {
                get { return sifraLeta; }
                set { sifraLeta = value; OnProperyChanged("SifraLeta"); }
            }

            private String brojSedista;

            public string BrojSedista
            {
                get { return brojSedista ; }
                set { brojSedista = value; OnProperyChanged("BrojSedista"); }
            }



            private String putnik;

            public string Putnik
            {
                get { return putnik; }
                set { putnik = value; OnProperyChanged("Putnik"); }
            }

        private String klasaSedista;

        public string KlasaSedista
        {
            get { return klasaSedista; }
            set { klasaSedista = value; OnProperyChanged("KlasaSedista"); }
        }

        private decimal cena;

        public decimal Cena
        {
            get { return cena; }
            set { cena = value; OnProperyChanged("Cena"); }
        }


        private int id;

            public int Id
            {
                get { return id; }
                set { id = value; OnProperyChanged("Id"); }
            }

            private bool aktivan;



            public bool Aktivan
            {
                get { return aktivan; }
                set { aktivan = value; }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnProperyChanged(string name)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(name));
                }
            }

            public override string ToString()
            {
                return $"{SifraLeta}";
            }


            public object Clone()
            {
                Karta karta = new Karta()
                {
                    SifraLeta = this.SifraLeta,
                    BrojSedista = this.BrojSedista,
                    Putnik = this.Putnik,
                    KlasaSedista = this.KlasaSedista,
                    Cena = this.Cena,
                    Aktivan = this.Aktivan,
                    Id = this.Id
                };
                return karta;
            }

        public void SacuvajKartu()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Karte(SifraLeta,BrojSedista,Putnik,KlasaSedista,Cena,Aktivan)"
                                    + "VALUES(@SifraLeta,@BrojSedista,@Putnik,@KlasaSedista,@Cena,@Aktivan)";

                command.Parameters.Add(new SqlParameter("@SifraLeta", this.SifraLeta));
                command.Parameters.Add(new SqlParameter("@BrojSedista", this.BrojSedista));
                command.Parameters.Add(new SqlParameter("@Putnik", this.Putnik));
                command.Parameters.Add(new SqlParameter("@KlasaSedista", this.KlasaSedista));
                command.Parameters.Add(new SqlParameter("@Cena", this.cena));
                command.Parameters.Add(new SqlParameter("@Aktivan", true));

                command.ExecuteNonQuery();
            }
            Database.Data.Instance.UcitajSveKarte();
        }
    }
}
