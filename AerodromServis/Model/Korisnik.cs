﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace AerodromWPF.Model
{
    public class Korisnik : INotifyPropertyChanged, ICloneable
    {
        public enum ETipKorisnika { KORISNIK, ADMINISTRATOR };
        public enum EPol {MUSKI,ZENSKI};

        public event PropertyChangedEventHandler PropertyChanged;



        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public Korisnik()
        {
            Aktivan = true;
        }

        private String ime;

        public String Ime
        {
            get { return ime; }
            set { ime = value; OnProperyChanged("Ime"); }
        }

        private String prezime;

        public String Prezime
        {
            get { return prezime; }
            set { prezime = value; OnProperyChanged("Prezime"); }
        }

        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; OnProperyChanged("Email"); }
        }

        private String adresa;

        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; OnProperyChanged("Adresa"); }
        }

        private EPol pol;

        public EPol Pol
        {
            get { return pol; }
            set { pol = value; OnProperyChanged("Pol"); }

        }

        private String korisnickoIme;

        public String KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; OnProperyChanged("KorisnickoIme"); }
        }

        private String lozinka;

        public String Lozinka
        {
            get { return lozinka; }
            set { lozinka = value; OnProperyChanged("Lozinka"); }
        }

        private ETipKorisnika tipK;

        public ETipKorisnika TipK
        {
            get { return tipK; }
            set { tipK = value; OnProperyChanged("TipK"); }
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("Id"); }
        }


        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }

        }

        public object Clone()
        {
            return new Korisnik
            {
                Ime = this.Ime,
                Prezime = this.Prezime,
                KorisnickoIme = this.KorisnickoIme,
                Lozinka = this.Lozinka,
                Adresa = this.Adresa,
                Pol = this.Pol,
                Email = this.Email,
                TipK = this.TipK,
                Aktivan = this.Aktivan,
                Id = this.Id
            };
        }
        public override string ToString()
        {
            return $"{KorisnickoIme}";
        }


        public void SacuvajKorisnika()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO KorisniciB(Ime,Prezime,KorisnickoIme,Sifra,Adresa,Pol,E_mail,TipKoris,Aktivan)" 
                                    + "VALUES(@Ime,@Prezime,@KorisnickoIme,@Lozinka,@Adresa,@Pol,@Email,@TipK,@Aktivan)";

                command.Parameters.Add(new SqlParameter("@Ime", this.Ime));
                command.Parameters.Add(new SqlParameter("@Prezime", this.Prezime));
                command.Parameters.Add(new SqlParameter("@KorisnickoIme", this.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Lozinka", this.Lozinka));
                command.Parameters.Add(new SqlParameter("@Adresa", this.Adresa));
                command.Parameters.Add(new SqlParameter("@Pol", this.Pol));
                command.Parameters.Add(new SqlParameter("@Email", this.Email));
                command.Parameters.Add(new SqlParameter("@TipK", this.TipK));
                command.Parameters.Add(new SqlParameter("@Aktivan", true));

                command.ExecuteNonQuery();
            }
            Database.Data.Instance.UcitajSveKorisnike();
        }

        public void IzmenaKorisnika()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update KorisniciB set Ime=@Ime,Prezime=@Prezime,KorisnickoIme=@KorisnickoIme,Sifra=@Lozinka,Adresa=@Adresa,Pol=@Pol,E_mail=@Email,TipKoris=@TipK, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Ime", this.Ime));
                comm.Parameters.Add(new SqlParameter("@Prezime", this.Prezime));
                comm.Parameters.Add(new SqlParameter("@KorisnickoIme", this.KorisnickoIme));
                comm.Parameters.Add(new SqlParameter("@Lozinka", this.Lozinka));
                comm.Parameters.Add(new SqlParameter("@Adresa", this.Adresa));
                comm.Parameters.Add(new SqlParameter("@Pol", this.Pol));
                comm.Parameters.Add(new SqlParameter("@Email", this.Email));
                comm.Parameters.Add(new SqlParameter("@TipK", this.TipK));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveKorisnike();
        }
        public void BrisanjeKorisnika()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update KorisniciB set Ime=@Ime,Prezime=@Prezime,KorisnickoIme=@KorisnickoIme,Sifra=@Lozinka,Adresa=@Adresa,Pol=@Pol,E_mail=@Email,TipKoris=@TipK, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Ime", this.Ime));
                comm.Parameters.Add(new SqlParameter("@Prezime", this.Prezime));
                comm.Parameters.Add(new SqlParameter("@KorisnickoIme", this.KorisnickoIme));
                comm.Parameters.Add(new SqlParameter("@Lozinka", this.Lozinka));
                comm.Parameters.Add(new SqlParameter("@Adresa", this.Adresa));
                comm.Parameters.Add(new SqlParameter("@Pol", this.Pol));
                comm.Parameters.Add(new SqlParameter("@Email", this.Email));
                comm.Parameters.Add(new SqlParameter("@TipK", this.TipK));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan = false));

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveKorisnike();
        }

    }

}
