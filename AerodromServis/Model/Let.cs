﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AerodromWPF.Model
{
    public class Let : INotifyPropertyChanged, ICloneable
    {

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public Let()
        {
            Aktivan = true;
        }

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnProperyChanged("Sifra"); }
        }

        private String odrediste;

        public string Odrediste
        {
            get { return odrediste; }
            set { odrediste = value; OnProperyChanged("Odrediste"); }
        }

        private String destinacija;

       
        public String Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; OnProperyChanged("Destinacija"); }
        }

        private DateTime vremePolaska;

        public DateTime VremePolaska
        {
            get { return vremePolaska; }
            set { vremePolaska = value; OnProperyChanged("VremePolaska"); }
        }

        private DateTime vremeDolaska;

        public DateTime VremeDolaska
        {
            get { return vremeDolaska; }
            set { vremeDolaska = value; OnProperyChanged("VremeDolaska"); }
        }

        private decimal cena;



        public decimal Cena
        {
            get { return cena; }
            set { cena = value; }
        }

        private String nazivAvioK;

        public string NazivAvioK
        {
            get { return nazivAvioK; }
            set { nazivAvioK = value; OnProperyChanged("NazivAvioK"); }
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("Id"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }


        public object Clone()
        {
            return new Let
            {
                Sifra = this.Sifra,
                Odrediste = this.Odrediste,
                Destinacija = this.Destinacija,
                VremePolaska = this.VremePolaska,
                VremeDolaska = this.VremeDolaska,
                Cena = this.Cena,
                NazivAvioK = this.NazivAvioK,
                Id = this.Id,
                Aktivan = this.Aktivan
            };
        }

        public override string ToString()
        {
            return $"  Odrediste:  {Odrediste} Destinacija:  {Destinacija} Polazak:  { VremePolaska} Dolazak:  {VremeDolaska} Cena:  {Cena} Aviokompanija:  {NazivAvioK}";
        }

        public void SacuvajLet()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO LetoviB(Sifra_Let,Odrediste,Destinacija,Vreme_Polaska,Vreme_Dolaska,Cena,NazivAvioK,Aktivan)"
                    + "VALUES(@Sifra,@Odrediste,@Destinacija,@VremePolaska,@VremeDolaska,@Cena,@NazivAvioK,@Aktivan)";

                command.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@Odrediste", this.Odrediste));
                command.Parameters.Add(new SqlParameter("@Destinacija", this.Destinacija));
                command.Parameters.Add(new SqlParameter("@VremePolaska", this.VremePolaska));
                command.Parameters.Add(new SqlParameter("@VremeDolaska", this.VremeDolaska));
                command.Parameters.Add(new SqlParameter("@Cena", this.Cena));
                command.Parameters.Add(new SqlParameter("@NazivAvioK", this.NazivAvioK));
                command.Parameters.Add(new SqlParameter("@Aktivan", true));

                command.ExecuteNonQuery();
            }
            Database.Data.Instance.UcitajSveLetove();
        }
        public void IzmenaLetova()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update LetoviB set Sifra_Let=@Sifra,Odrediste=@Odrediste,Destinacija=@Destinacija,Vreme_Polaska=@VremePolaska,Vreme_Dolaska=@VremeDolaska,Cena=@Cena, NazivAvioK=@NazivAvioK, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@Odrediste", this.Odrediste));
                comm.Parameters.Add(new SqlParameter("@Destinacija", this.Destinacija));
                comm.Parameters.Add(new SqlParameter("@VremePolaska", this.VremePolaska));
                comm.Parameters.Add(new SqlParameter("@VremeDolaska", this.VremeDolaska));
                comm.Parameters.Add(new SqlParameter("@Cena", this.Cena));
                comm.Parameters.Add(new SqlParameter("@NazivAvioK", this.NazivAvioK));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan));

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveLetove();
        }
        public void BrisanjeLetova()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Database.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update LetoviB set Sifra_Let=@Sifra,Odrediste=@Odrediste,Destinacija=@Destinacija,Vreme_Polaska=@VremePolaska,Vreme_Dolaska=@VremeDolaska,Cena=@Cena, NazivAvioK=@NazivAvioK, Aktivan=@Aktivan where Id=@Id";

                comm.Parameters.Add(new SqlParameter("@Id", this.Id));
                comm.Parameters.Add(new SqlParameter("@Sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@Odrediste", this.Odrediste));
                comm.Parameters.Add(new SqlParameter("@Destinacija", this.Destinacija));
                comm.Parameters.Add(new SqlParameter("@VremePolaska", this.VremePolaska));
                comm.Parameters.Add(new SqlParameter("@VremeDolaska", this.VremeDolaska));
                comm.Parameters.Add(new SqlParameter("@Cena", this.Cena));
                comm.Parameters.Add(new SqlParameter("@NazivAvioK", this.NazivAvioK));
                comm.Parameters.Add(new SqlParameter("@Aktivan", this.Aktivan = false));

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Database.Data.Instance.UcitajSveLetove();
        }
    }

}
