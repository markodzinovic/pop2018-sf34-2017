﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Sedista : INotifyPropertyChanged, ICloneable
    {
        private string ime;

        public string Ime
        {
            get { return ime; }
            set { ime = value; Changed("Ime"); }
        }
        private Boolean zauzeto;

        public Boolean Zauzeto
        {
            get { return zauzeto; }
            set { zauzeto = value; Changed("Zauzeto"); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }


        public object Clone()
        {
            Sedista sedista = new Sedista
            {
                Ime = this.ime,
                Zauzeto = this.zauzeto
            };
            return sedista;
        }


    }
}
