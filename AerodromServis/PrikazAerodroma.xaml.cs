﻿using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for PrikazAerodroma.xaml
    /// </summary>
    public partial class PrikazAerodroma : Window
    {
        private Aerodrom aerodrom;
        public PrikazAerodroma(Aerodrom aerodrom)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;


            TxtSifra.Text = aerodrom.Sifra;
            TxtGrad.Text = aerodrom.Grad;
            TxtNaziv.Text = aerodrom.Naziv;

            TxtSifra.IsEnabled = false;
            TxtGrad.IsEnabled = false;
            TxtNaziv.IsEnabled = false;
        }
    }
}
