﻿#pragma checksum "..\..\DodavanjeIzmenaK.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "04282E7F43613FC2DC15556C87EFED10D43E8689"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AerodromWPF;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AerodromWPF {
    
    
    /// <summary>
    /// DodavanjeIzmenaK
    /// </summary>
    public partial class DodavanjeIzmenaK : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblIme;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtIme;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPrezime;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtPrezime;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtKorisnickoIme;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtSifra;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtEmail;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtAdresa;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblKorisnickoIme;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSifra;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblEmail;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAdresa;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cPol;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cTip;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSacuvaj;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\DodavanjeIzmenaK.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOdustani;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AerodromWPF;component/dodavanjeizmenak.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\DodavanjeIzmenaK.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lblIme = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.TxtIme = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.lblPrezime = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.TxtPrezime = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.TxtKorisnickoIme = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.TxtSifra = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.TxtEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.TxtAdresa = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.lblKorisnickoIme = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lblSifra = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.lblEmail = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.lblAdresa = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.cPol = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 14:
            this.cTip = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 15:
            this.btnSacuvaj = ((System.Windows.Controls.Button)(target));
            return;
            case 16:
            this.btnOdustani = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

