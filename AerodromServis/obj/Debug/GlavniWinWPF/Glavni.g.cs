﻿#pragma checksum "..\..\..\GlavniWinWPF\Glavni.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "F9D9B352F074585373E83C8EB72F05250626DE11"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AerodromWPF.GlavniWinWPF;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AerodromWPF.GlavniWinWPF {
    
    
    /// <summary>
    /// Glavni
    /// </summary>
    public partial class Glavni : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cDestinacijaPr;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cOdredistePro;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtDo;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtDest;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtOdred;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbBiznis;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbEkonomska;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPretraga;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox listLetovi;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtKorisnickoIme;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox pswdSifra;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPrijava;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\GlavniWinWPF\Glavni.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnRegistracija;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AerodromWPF;component/glavniwinwpf/glavni.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\GlavniWinWPF\Glavni.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.cDestinacijaPr = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 2:
            this.cOdredistePro = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.txtDo = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.txtDest = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.txtOdred = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.rbBiznis = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 7:
            this.rbEkonomska = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 8:
            this.btnPretraga = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\GlavniWinWPF\Glavni.xaml"
            this.btnPretraga.Click += new System.Windows.RoutedEventHandler(this.btnPretraga_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.listLetovi = ((System.Windows.Controls.ListBox)(target));
            return;
            case 10:
            this.txtKorisnickoIme = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.pswdSifra = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 12:
            this.btnPrijava = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\..\GlavniWinWPF\Glavni.xaml"
            this.btnPrijava.Click += new System.Windows.RoutedEventHandler(this.btnPrijava_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.btnRegistracija = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\..\GlavniWinWPF\Glavni.xaml"
            this.btnRegistracija.Click += new System.Windows.RoutedEventHandler(this.btnRegistracija_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

