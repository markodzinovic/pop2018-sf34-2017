﻿#pragma checksum "..\..\KarteADDEDIt.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "144F00DE67E220A769FC12C7D92ACB30F6374442"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AerodromWPF;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AerodromWPF {
    
    
    /// <summary>
    /// KarteADDEDIt
    /// </summary>
    public partial class KarteADDEDIt : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtSifraLeta;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtCena;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtOdred;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtDest;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtVremePola;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtVremeDola;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Sedista;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtKlasa;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDodaj;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOdustani;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\KarteADDEDIt.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cKorisnickoIme;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AerodromWPF;component/karteaddedit.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\KarteADDEDIt.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txtSifraLeta = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.txtCena = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.txtOdred = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.txtDest = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.txtVremePola = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.txtVremeDola = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.Sedista = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.txtKlasa = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.btnDodaj = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\KarteADDEDIt.xaml"
            this.btnDodaj.Click += new System.Windows.RoutedEventHandler(this.btnDodaj_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnOdustani = ((System.Windows.Controls.Button)(target));
            
            #line 28 "..\..\KarteADDEDIt.xaml"
            this.btnOdustani.Click += new System.Windows.RoutedEventHandler(this.btnOdustani_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.cKorisnickoIme = ((System.Windows.Controls.ComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

